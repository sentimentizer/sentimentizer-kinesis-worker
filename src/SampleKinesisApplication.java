/*
 * Copyright 2012-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;

/**
 * Sample Kinesis Application.
 */
public final class SampleKinesisApplication {
	private static final Logger LOGGER = Logger.getLogger("KinesisApplication");
	private static CommandLine cli;
	public static Config config;
	public static String workerID;
    private static KinesisClientLibConfiguration kinesisClientLibConfiguration;    
    
    /**
     * 
     */
    private SampleKinesisApplication() {
        super();
    }
    
    /**
     * @param args Property file with config overrides (e.g. application name, stream name)
     * @throws IOException Thrown if we can't read properties from the specified properties file
     */
    public static void main(String[] args) throws IOException {
		// Cheating the log4j configuration - should be in a config file
		// But feeling lazy
		final Properties logProperties = new Properties();
		logProperties.setProperty("log4j.rootLogger", "INFO, A1");
		logProperties.setProperty("log4j.appender.A1", "org.apache.log4j.ConsoleAppender");
		logProperties.setProperty("log4j.appender.A1.layout", "org.apache.log4j.PatternLayout");
		logProperties.setProperty("log4j.appender.A1.layout.ConversionPattern", "%d [%t] %-5p %c{2} - %m%n");
		PropertyConfigurator.configure(logProperties);
		
		if (!parseCLI(args)) { // Some error has occurred parsing the command line arguments, we exit...
			System.exit(1);
		}
		
		// ensure the JVM will refresh the cached IP values of AWS resources (e.g. service endpoints).
        java.security.Security.setProperty("networkaddress.cache.ttl" , "60");
        
        workerID = InetAddress.getLocalHost().getCanonicalHostName() + ":" + UUID.randomUUID();
        
		// Get credentials from IMDS. If unsuccessful, get them from the classpath. 
	       AWSCredentialsProvider credentialsProvider = null;
	        try {
	            credentialsProvider = new InstanceProfileCredentialsProvider();
	            // Verify we can fetch credentials from the provider
	            credentialsProvider.getCredentials();
	            LOGGER.info("Obtained credentials from the IMDS.");
	        } catch (AmazonClientException e) {
	            LOGGER.warn("Unable to obtain credentials from the IMDS, trying classpath properties");
	            credentialsProvider = new ClasspathPropertiesFileCredentialsProvider();
	            // Verify we can fetch credentials from the provider
	            credentialsProvider.getCredentials();
	            LOGGER.info("Obtained credentials from the properties file.");
	        }
	        
	        LOGGER.info("Using credentials with access key id: " + credentialsProvider.getCredentials().getAWSAccessKeyId());
	        
        kinesisClientLibConfiguration = new KinesisClientLibConfiguration(config.getApplicationName(), config.getStreamName(), 
        		credentialsProvider, workerID).withInitialPositionInStream(config.getInitialPositionInStream().toUpperCase().contentEquals("LATEST") ? InitialPositionInStream.LATEST : InitialPositionInStream.TRIM_HORIZON); 

		        
        LOGGER.info("Running ("+config.getApplicationName()+") to process stream ("+config.getStreamName()+") workerID("+workerID+")");
          
        IRecordProcessorFactory recordProcessorFactory = new SampleRecordProcessorFactory();
         Worker worker = new Worker(recordProcessorFactory, kinesisClientLibConfiguration);

        int exitCode = 0;
        try {
            worker.run();
        } catch (Throwable t) {
            LOGGER.error("Caught throwable while processing data.", t);
            exitCode = 1;
        }
        System.exit(exitCode);
    }
    
	/**
	 * Parses the Command Line Interface arguments and sets up the configuration
	 * @param args Array of command line arguments
	 * 
	 * @return true if no errors, false if an error is found in the CLI arguments
	 */
	@SuppressWarnings("static-access")
	private static boolean parseCLI(String[] args) {
		Options options = new Options();
		
		options.addOption(OptionBuilder.withLongOpt("config").withDescription("Configuration file in JSON format").hasArg().withArgName("FILE").create('c'));
		
		CommandLineParser parser = new BasicParser();
		try {
			cli = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println(e.getLocalizedMessage());
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("KinesisApplication", options);
			return false;
		}
		
		boolean errorFlag = false;	// We set this to true below if any errors are found.
		
		if (cli.hasOption("config")) {
			// Read in the configuration
			try {
				config(cli.getOptionValue("config"));
			} catch (Exception e) {
				LOGGER.fatal("Reading configuration failed. Exiting...");
				errorFlag = true;
			}
		} else {
			LOGGER.fatal("--config must be specified!");
			errorFlag = true;
		}
					
		if (errorFlag) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("KinesisApplication", options);
			return false;
		}
		return true;
	}
	
	private static void config(String fileName) throws Exception {
		config = new Config(fileName);
	}
}
