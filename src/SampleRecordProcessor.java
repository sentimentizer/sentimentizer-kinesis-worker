/*
 * Copyright 2012-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.amazonaws.services.kinesis.clientlibrary.exceptions.InvalidStateException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ShutdownException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ThrottlingException;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownReason;
import com.amazonaws.services.kinesis.model.Record;
import com.google.gson.Gson;

import java.util.*;
import java.io.IOException;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.MemcachedClient; 
import net.spy.memcached.internal.OperationCompletionListener;
import net.spy.memcached.internal.OperationFuture;


/**
 *
 */
public class SampleRecordProcessor implements IRecordProcessor {
	private static final Logger LOGGER = Logger.getLogger("RecordProcessor");
	private String kinesisShardId;

	// Backoff and retry settings
	private static final long BACKOFF_TIME_IN_MILLIS = 3000L;
	private static final int NUM_RETRIES = 10;

	// Checkpoint about once a minute
	private static final long CHECKPOINT_INTERVAL_MILLIS = 60000L;
	private long nextCheckpointTimeInMillis;

	private final CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();

	private MemcachedClient client;

	private Stats stats;
	private String topicKey = "";
	private int unsigned_vote = 0;


	private String data = null;
	private final Gson gson = new Gson();
	private Vote vote;
	private Listener listener;

	/**
	 * Constructor.
	 */
	public SampleRecordProcessor() {
		super();

		try {
			client = new MemcachedClient(AddrUtil.getAddresses(SampleKinesisApplication.config.getMemcache()));
			
			listener = new Listener(client, this);
			LOGGER.info("Creating memcached client");

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize(final String shardId) {
		stats = new Stats(SampleKinesisApplication.config.getCentralStats(), shardId, this);
		LOGGER.info("Initializing record processor for shard: " + shardId);
		this.kinesisShardId = shardId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processRecords(final List<Record> records, final IRecordProcessorCheckpointer checkpointer) {
		LOGGER.info("Processing " + records.size() + " records from " + kinesisShardId);

		// Process records and perform all exception handling.
		processRecordsWithRetries(records);

		// Checkpoint once every checkpoint interval.
		if (System.currentTimeMillis() > nextCheckpointTimeInMillis) {
			checkpoint(checkpointer);
			nextCheckpointTimeInMillis = System.currentTimeMillis() + CHECKPOINT_INTERVAL_MILLIS;
		}
	}

	/** Process records performing retries as needed. Skip "poison pill" records.
	 * @param records
	 */
	private void processRecordsWithRetries(List<Record> records) {
		LOGGER.info("Record Count to process " + records.size() );
		boolean processedSuccessfully;
		for (Record record : records) {
			//stats.incRecordCount(kinesisShardId);

			processedSuccessfully = false;
			try {
				// For this app, we interpret the payload as UTF-8 chars.
				data = decoder.decode(record.getData()).toString();
				//LOG.info(record.getSequenceNumber() + ", " + record.getPartitionKey() + ", " + data);

				// LOG.info("firstchar:" + data.substring(0,1));

				if (data.substring(0,1).equals("{") == false) {
					byte[] decoded = Base64.decodeBase64(data.getBytes(Charset.forName("UTF-8")));
					data = new String(decoded, "UTF-8");
					LOGGER.warn("Decoded Data:" + data);
				} 


				// Logic to process record goes here.
				//
				//Going to parse the JSON payload and get the component parts


				vote = gson.fromJson(data, Vote.class);
				//LOG.info("data:" + data);
				//LOG.info("vote:" + vote.rating);

				////////////////////////////////////////////
				//START Code to load data for Pulse application
				//////////////////////////////////////////

				topicKey = "sentimentVotes." + vote.topic;

				//Write to memcached
				incr(topicKey, 1);


				//If the sentiment is positive, add it to positive otherwise add it to negative

				if (vote.rating >= 0) {
					topicKey = "sentimentTotalPositive." + vote.topic;
					incr(topicKey, vote.rating);

				} else {
					//Increment will only with positive number - so need to remove the sign.
					unsigned_vote = Math.abs(vote.rating);

					topicKey = "sentimentTotalNegative." + vote.topic;
					incr(topicKey, unsigned_vote);
				}




				////////////////////////////////////////////
				//START Code to load data for Tealeaves application
				//////////////////////////////////////////

				
				topicKey = "distribution." + vote.topic + "." + Integer.toString(vote.rating);

				incr(topicKey, 1);

				processedSuccessfully = true;
				stats.incRecordCount();
			} catch (CharacterCodingException e) {
				LOGGER.error("Malformed data: " + data, e);
			} catch (Throwable t) {
				LOGGER.warn("Caught throwable while processing record " + record, t);
			}

			if (!processedSuccessfully) {
				LOGGER.error("Couldn't process record " + record + ". Skipping the record.");
			}
			// */
		}   		     
	}

	private void incr(final String key, final long value) {
		/*
    	//Write to memcached
        if (client.incr(key, value) == -1) {
        	//Create key if it does not exist
        	client.add(key,3600, Long.toString(value));
        	//LOG.info("Added item for: " + topicKey);
    	} else {
    		//LOG.info("Incremented item for: " + topicKey);
    	}
		 */
		//stats.incrCount++;
		// client.asyncIncr(key, value).addListener(listener);
		stats.incKey(key, (int)value);	// Used to send results to central stats server
		//stats.incrCount++;
	}
	
	public void updateCache(final String key, final long value) {
		client.asyncIncr(key, value).addListener(listener);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void shutdown(IRecordProcessorCheckpointer checkpointer, ShutdownReason reason) {
		LOGGER.info("Shutting down record processor for shard: " + kinesisShardId);
		// Important to checkpoint after reaching end of shard, so we can start processing data from child shards.
		if (reason == ShutdownReason.TERMINATE) {
			checkpoint(checkpointer);
		}
	}



	/** Checkpoint with retries.
	 * @param checkpointer
	 */
	private void checkpoint(IRecordProcessorCheckpointer checkpointer) {
		LOGGER.info("Checkpointing shard " + kinesisShardId);
		for (int i = 0; i < NUM_RETRIES; i++) {
			try {
				checkpointer.checkpoint();
				break;
			} catch (ShutdownException se) {
				// Ignore checkpoint if the processor instance has been shutdown (fail over).
				LOGGER.info("Caught shutdown exception, skipping checkpoint.", se);
				break;
			} catch (ThrottlingException e) {
				// Backoff and re-attempt checkpoint upon transient failures
				if (i >= (NUM_RETRIES - 1)) {
					LOGGER.error("Checkpoint failed after " + (i + 1) + "attempts.", e);
					break;
				} else {
					LOGGER.info("Transient issue when checkpointing - attempt " + (i + 1) + " of "
							+ NUM_RETRIES, e);
				}
			} catch (InvalidStateException e) {
				// This indicates an issue with the DynamoDB table (check for table, provisioned IOPS).
				LOGGER.error("Cannot save checkpoint to the DynamoDB table used by the Amazon Kinesis Client Library.", e);
				break;
			}
			try {
				Thread.sleep(BACKOFF_TIME_IN_MILLIS);
			} catch (InterruptedException e) {
				LOGGER.debug("Interrupted sleep", e);
			}
		}
	}

}

/**
 * This acts as a listener for Memcache async connections
 * If not successful we add a new key.
 * @author goreg
 *
 */
class Listener implements OperationCompletionListener {
	private final MemcachedClient client;
	private final SampleRecordProcessor rp;
	
	public Listener(final MemcachedClient client, SampleRecordProcessor rp) {
		this.client = client;
		this.rp = rp;
	}
	
	@Override
	public void onComplete(OperationFuture<?> future) throws Exception {
		if (!future.getStatus().isSuccess()) {
			client.add(future.getKey(), 86400, "1");	// FIXME This means the first vote is always set to 1, need a better way to handle this
		}
	}
}

/**
 * GSON POJO for working with the data
 * @author goreg
 *
 */
class Vote {
	String timestamp;
	String topic;
	int rating;
	String useragent;

	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getUseragent() {
		return useragent;
	}
	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}
}