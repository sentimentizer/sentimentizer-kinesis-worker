

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 * Generic Redis client
 * 
 * @author goreg
 *
 */
public class RedisClient {
	private static final Logger LOGGER = Logger.getLogger(RedisClient.class);
	private final String host;
	private final int port;
	private Jedis jedis = null;
	
	public RedisClient(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	private void connect() {
		if (jedis != null) {
			close();
		}
		jedis = new Jedis(host, port);
	}
	
	private void close() {
		try {
			jedis.quit();
		} catch (Exception e) { }
	}
	
	public void start() {
		LOGGER.info("connecting to redis server ("+host+":"+port+")");
		connect();
	}
	
	/**
	 * Retrieves an entry from redis, as specified by list index
	 * @param index	position in the list
	 * @return msg text
	 */
	public String lindex(final String list, final int index) {
		synchronized (jedis) {
			try {
				String s = jedis.lindex(list, index);
				return s;
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis lindex() error: "+e.getLocalizedMessage());
				connect();
				return lindex(list, index);
			}
		}
	}
	
	public long llen(final String key) {
		synchronized (jedis) {
			try {
				return jedis.llen(key);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis llen() error: "+e.getLocalizedMessage());
				connect();
				return llen(key);
			}
		}
	}

	public long del(final String key) {
		synchronized (jedis) {
			try {
				return jedis.del(key);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis del() error: "+e.getLocalizedMessage());
				connect();
				return del(key);
			}
		}		
	}

	public String get(final String key) {
		synchronized (jedis) {
			try {
				return jedis.get(key);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis get() error: "+e.getLocalizedMessage());
				connect();
				return get(key);
			}
		}		
	}

	public String hget(final String key, final String field) {
		synchronized (jedis) {
			try {
				return jedis.hget(key, field);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis hget() error: "+e.getLocalizedMessage());
				connect();
				return hget(key, field);
			}
		}		
	}
	
	public long hset(final String key, final String field, final String value) {
		synchronized (jedis) {
			try {
				return jedis.hset(key, field, value);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis hset() error: "+e.getLocalizedMessage());
				connect();
				return hset(key, field, value);
			}
		}		
	}
	
	public String hmset(final String key, final Map<String, String> hash) {
		synchronized (jedis) {
			try {
				return jedis.hmset(key, hash);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis hmset() error: "+e.getLocalizedMessage());
				connect();
				return hmset(key, hash);
			}
		}		
	}
	
	public long lpush(final String key, final String value) {
		synchronized (jedis) {
			try {
				return jedis.lpush(key, value);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis lpush() error: "+e.getLocalizedMessage());
				connect();
				return lpush(key, value);
			}
		}		
	}
	
	public long rpush(final String key, final String value) {
		synchronized (jedis) {
			try {
				return jedis.rpush(key, value);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis rpush() error: "+e.getLocalizedMessage());
				connect();
				return rpush(key, value);
			}
		}		
	}
	
	public void ltrim(final String key, final int start, final int stop) {
		synchronized (jedis) {
			try {
				jedis.ltrim(key, start, stop);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis ltrim() error: "+e.getLocalizedMessage());
				connect();
				ltrim(key, start, stop);
			}
		}		
	}
	
	public Set<String> keys(final String pattern) {
		synchronized (jedis) {
			try {
				return jedis.keys(pattern);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis keys() error: "+e.getLocalizedMessage());
				connect();
				return keys(pattern);
			}
		}		
	}

	public long hincrby(final String key, final String field, final long value) {
		synchronized (jedis) {
			try {
				return jedis.hincrBy(key, field, value);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis keys() error: "+e.getLocalizedMessage());
				connect();
				return hincrby(key, field, value);
			}
		}		
	}
	public long incrby(final String key, final long value) {
		synchronized (jedis) {
			try {
				return jedis.incrBy(key, value);
			} catch (JedisConnectionException e) {
				LOGGER.warn("redis incrby() error: "+e.getLocalizedMessage());
				connect();
				return incrby(key, value);
			}
		}		
	}
}