import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

/**
 * Configuration class, reads a JSON config file and sets up all the configuration variables
 * @author goreg
 *
 */
public class Config {
	private static final Logger LOGGER = Logger.getLogger(Config.class);
	private _Config config;	

	/**
	 * Read in a JSON config file
	 * @param fileName
	 * @throws Exception 
	 */
	public Config (String fileName) throws Exception {
		try {
			File file = new java.io.File(fileName);
			if (file.length() == 0 || file.length() > 1024) {
				LOGGER.error("Length of file is incorrect. Can't be 0 or > 1,024!");
				throw new Exception("Length of file is incorrect. Can't be 0 or > 1,024!");
			}
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				if (! line.trim().startsWith("//")) {	// Deals with commented out lines
					sb.append(line);
				}
			}
			br.close();
			
			Gson gson = new Gson();
			config = gson.fromJson(sb.toString(), _Config.class);
		} catch (FileNotFoundException e) {
			LOGGER.error("Couldn't open file ("+fileName+"). Reason: "+e.getLocalizedMessage());
			throw new Exception("Couldn't open file ("+fileName+")");
		} catch (Exception e) {
			LOGGER.error("Exception caught. "+e.getClass().getName()+" Reason: "+e.getLocalizedMessage());
			throw new Exception("Generic Exception Error");
		}
	}
	
	public String getApplicationName() {
		return config.getApplicationName();
	}
	
	public String getInitialPositionInStream() {
		return config.getInitialPositionInStream();
	}
	
	public String getStreamName() {
		return config.getStreamName();
	}
	
	public String getMemcache() {
		return config.getMemcache();
	}
	
	public String getCentralStats() {
		return config.getCentralStats();
	}
}

class _Config {
	private String applicationName;
	private String initialPositionInStream;
	private String streamName;
	private String memcache;
	private String centralStats;
	
	public String getCentralStats() {
		return centralStats;
	}

	public void setCentralStats(String centralStats) {
		this.centralStats = centralStats;
	}

	public String getMemcache() {
		return memcache;
	}

	public void setMemcache(String memcache) {
		this.memcache = memcache;
	}

	public _Config() { }
	
	public void setApplicationName(final String applicationName) {
		this.applicationName = applicationName;
	}

	public String getInitialPositionInStream() {
		return initialPositionInStream;
	}

	public String getStreamName() {
		return streamName;
	}

	public void setStreamName(final String streamName) {
		this.streamName = streamName;
	}

	public void setInitialPositionInStream(final String initialPositionInStream) {
		this.initialPositionInStream = initialPositionInStream;
	}

	public String getApplicationName() {
		return applicationName;
	}
}
