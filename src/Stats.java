import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

public class Stats extends TimerTask {
	final private Logger LOGGER = Logger.getLogger(Stats.class);
	final private String host;
	final private int port;
	final private String projectID;
	private long count = 0;
	private long oldCount = 0;
	private final String uuid;	// String representing this client
	private RedisClient redis;
	private final String shardID;
	private Object lock = new Object();
	private HashMap<String, Long> hash;
	private HashMap<String, Long> oldHash;
	private SampleRecordProcessor rp;

	public Stats(final String statsURL, final String shardID, final SampleRecordProcessor rp) {
		this.shardID = shardID;
		this.rp = rp;
		
		String t = "http://"+statsURL;
		try {
			URL url = new URL(t);
			this.host = url.getHost();
			this.port = url.getPort() != -1 ? url.getPort() : 6379;
			this.projectID = url.getPath().substring(1, url.getPath().length()).toUpperCase();	// Strip the leading / and convert to UPPER-CASE

			this.uuid = SampleKinesisApplication.workerID;
			LOGGER.info("Connecting to Stats server ("+host+":"+port+") project ("+projectID+") client uuid ("+uuid+")");
			redis = new RedisClient(host, port);
			redis.start();

			createProject();

		} catch (MalformedURLException e) {
			LOGGER.error("stats server url("+t+") is invalid! Reason: "+e.getLocalizedMessage());
			throw new IllegalArgumentException("Bad URL...");
		}

		hash = new HashMap<>();
		oldHash = new HashMap<>();
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(this, 1000, 333);	// Run 3 times a second
		LOGGER.info("Stats thread started...");
	}
	
	@Override
	public void run() {
		synchronized (lock ) {
			long delta = count - oldCount;
			oldCount += delta;
			redis.hincrby(projectID+"-KCL-shards", shardID, delta);
			long x;
			for (String key : hash.keySet()) {
				x = hash.get(key)-oldHash.get(key);
				oldHash.put(key, hash.get(key));
				redis.hincrby(projectID+"-VOTES", key, x);
				rp.updateCache(key, x);
			}
		}
	}
	
	private void createProject() {
		if (redis.hget(projectID, "dateCreated") == null) {
			// First time this project has been used - lets set the Date
			LOGGER.info("Setting up stats project("+projectID+") for the first time");
			redis.hset(projectID, "dateCreated", new DateTime().toString());
		}
	}
	
	public void incRecordCount() {
		synchronized (lock) {
			count++;
		}
	}
	
	public void incKey(final String key, final long value) {
		synchronized (lock) {
			if (hash.containsKey(key)) {
				hash.put(key, hash.get(key)+value);
			} else {
				hash.put(key, value);
				oldHash.put(key, 0L);
			}
		}
	}
}